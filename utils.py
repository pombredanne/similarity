import numpy as np
import pymongo

class DataReplacer:
    def __init__(self):
        self.user_counter = 0
        self.item_counter = 0
        self.user_id_to_index = {}
        self.user_index_to_id = {}
        self.item_id_to_index = {}
        self.item_index_to_id = {}

    def get_user_index(self, user_id):
        return self.user_id_to_index.get(user_id)

    def add_or_get_user_index(self, user_id):
        user_index = self.user_id_to_index.get(user_id)
        if user_index is None:
            user_index = self.user_counter
            self.user_id_to_index[user_id] = user_index
            self.user_index_to_id[user_index] = user_id
            self.user_counter += 1
            return user_index
        else:
            return user_index

    def get_user_id(self, user_index):
        return self.user_index_to_id[user_index]

    def get_item_index(self, item_id):
        return self.item_id_to_index.get(item_id)

    def add_or_get_item_index(self, item_id):
        item_index = self.item_id_to_index.get(item_id)
        if item_index is None:
            item_index = self.item_counter
            self.item_id_to_index[item_id] = item_index
            self.item_index_to_id[item_index] = item_id
            self.item_counter += 1
            return item_index
        else:
            return item_index

    def get_item_id(self, item_index):
        return self.item_index_to_id[item_index]


def get_max_values(similarity, cross_count, min_cross_count, count):
    res = []

    crosses_indexes = np.where(cross_count >= min_cross_count)[0]
    if crosses_indexes.size > 0:
        crosses_similarity = similarity[crosses_indexes]
        for i in range(0, min(count, crosses_indexes.size)):
            max = np.asscalar(crosses_similarity.max())
            arg_max = crosses_similarity.argmax()

            crosses_similarity[:, arg_max] = -100

            max_index = crosses_indexes[0, arg_max]
            max_count = cross_count[max_index][0][0, 0]
            res.append([max_index, None, max, max_count])
        return res, 'Found %s users with %s cross count' % (crosses_indexes.size, min_cross_count)

    else:
        return res, 'Not found users with this cross count: %s' % min_cross_count


def make_result(replacer, res, db, user_name):
    col = db.users
    index_to_delete = None
    user_ids = []
    if res:
        for i, r in enumerate(res):
            user_index = r[0]
            user_id = replacer.get_user_id(user_index)

            r[0] = user_id
            user_ids.append(user_id)

    if user_ids:
        users_docs = col.find({'_id': {'$in': user_ids}}, {'username': 1})
        users_names = {doc['_id']: doc['username'] for doc in users_docs}

        for i, r in enumerate(res):

            r[1] = users_names.get(r[0])
            index_to_delete = i if r[1] and r[1].lower() == user_name.lower() else index_to_delete

    res.pop(index_to_delete) if index_to_delete is not None else None
    return res
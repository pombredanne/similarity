import unittest
import numpy as np
from scipy.sparse import csr_matrix
from algorithms import CSRCosineSimilarity, NumpyCosineSimilarity, AbstractSimilarity, WrongInputData, OnlySharedCSRCosineSimilarity


class MatrixTest(unittest.TestCase):
    def setUp(self):
        list_matrix = [
            [1, 2, 3, 4, 5, 6, 2],
            [6, 0, 2, 0, 5, 1, 0],
            [1, 2, 0, 4, 5, 6, 7],
            [3, 0, 3, 8, 1, 5, 3],
            [5, 2, 0, 4, 5, 6, 7]
        ]
        self.np_matrix = np.matrix(list_matrix, dtype=np.float16)
        self.csr = csr_matrix(self.np_matrix, dtype=np.float16)
        self.hand_val = np.matrix([[0.54], [1], [0.4], [0.39], [0.6]])
        self.hand_count = np.matrix([[4], [4], [3], [4], [3]])
        self.shares_hand_val = np.matrix([[0.628], [0.999], [0.596], [0.630], [0.835]])
        self.one_row_index = 1

        self.CSRCosineSimilarity = CSRCosineSimilarity(self.csr)
        self.OnlySharedCSRCosineSimilarity = OnlySharedCSRCosineSimilarity(self.csr)
        self.NumpyCosineSimilarity = NumpyCosineSimilarity(self.np_matrix)

    def test_check_shapes(self):
        matrix_10_10 = csr_matrix(np.ones((10, 10), dtype=np.float16))
        matrix_9 = csr_matrix(np.ones(9, dtype=np.float16))
        csr = CSRCosineSimilarity(matrix_10_10)
        with self.assertRaises(WrongInputData):
            csr.count(matrix_9)

    def test_check_data_types(self):
        matrix_float16 = csr_matrix(np.ones((10, 10), dtype=np.float16))
        matrix_int32 = csr_matrix(np.ones(10, dtype=np.int32))
        csr = CSRCosineSimilarity(matrix_float16)
        with self.assertRaises(WrongInputData):
            csr.count(matrix_int32)

        matrix_int32 = csr_matrix(np.ones((10, 10), dtype=np.int32))
        with self.assertRaises(WrongInputData):
            CSRCosineSimilarity(matrix_int32)

    def test_check_matrix_types(self):
        numpy_matrix = np.ones((10, 10), dtype=np.float16)
        with self.assertRaises(WrongInputData):
            CSRCosineSimilarity(numpy_matrix)

        csr = csr_matrix(np.ones((10, 10), dtype=np.float16))
        numpy_matrix = np.ones(10, dtype=np.float16)
        obj = CSRCosineSimilarity(csr)
        with self.assertRaises(WrongInputData):
            obj.count(numpy_matrix)

    def test_simple_cosine(self):
        cosine_similarity_matrix, cross_count_matrix = self.NumpyCosineSimilarity.count(self.np_matrix[self.one_row_index])
        self.assertEqual(cosine_similarity_matrix.shape, (self.np_matrix.shape[0], 1L))
        self.assertTrue(abs((self.hand_val - cosine_similarity_matrix).sum()) < 0.01)
        self.assertTrue((self.hand_count == cross_count_matrix).all())

    def test_csr_cosine(self):
        cosine_similarity_matrix, cross_count_matrix = self.CSRCosineSimilarity.count(self.csr[self.one_row_index])
        self.assertEqual(cosine_similarity_matrix.shape, (self.csr.shape[0], 1L))
        self.assertTrue(abs((self.hand_val - cosine_similarity_matrix).sum()) < 0.01)
        self.assertTrue((self.hand_count == cross_count_matrix).all())

    def test_shared_csr_cosine(self):
        cosine_similarity_matrix, cross_count_matrix = self.OnlySharedCSRCosineSimilarity.count(self.csr[self.one_row_index])
        self.assertEqual(cosine_similarity_matrix.shape, (self.csr.shape[0], 1L))
        self.assertTrue(abs((self.shares_hand_val - cosine_similarity_matrix).sum()) < 0.01)
        self.assertTrue((self.hand_count == cross_count_matrix).all())

if __name__ == '__main__':
    unittest.main(verbosity=2)
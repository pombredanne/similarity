import time
import data_driver
import algorithms
import utils
from local_settings import *


neighbor_queue = site_db.neighbor_queue
neighbor_results = site_db.neighbor_results

Driver = data_driver.SparseDataLoader(data_db)
csr = Driver.get_csr_array()

cosine = algorithms.OnlySharedCSRCosineSimilarity(csr)

max_neighbour_count = 200
while True:
    queue = neighbor_queue.find()
    if queue.count() > 0:
        try:
            user_data = queue[0]
            user_name = user_data['_id']
            print user_name
            user_csr = Driver.get_avg_user_csr_array(user_data['scores'])
            similarity, cross_count = cosine.count(user_csr)
            result_list, message = utils.get_max_values(similarity, cross_count, user_data['cross_count'],
                                                        max_neighbour_count)

            prepared_result_list = utils.make_result(Driver.replacer, result_list, data_db, user_name)
            neighbor_queue.remove({'_id': user_name})
            neighbor_results.save({'_id': user_name, 'result': prepared_result_list, 'message': message,
                                   'cross_count': user_data['cross_count']})
        except BaseException as ex:
            neighbor_queue.remove({'_id': user_name})
            neighbor_results.save({'_id': user_name, 'result': [], 'message': ex.message})
    else:
        print 'wait'
        time.sleep(1)
